import VueRouter from 'vue-router'
import mainPage from '../pages/mainPage.vue'
import signIn from '../pages/auth/signIn.vue'
import signUp from '../pages/auth/signUp.vue'

export default new VueRouter({
  mode: 'history',
    routes:[
      {
        path: '/',
        component: mainPage
      },
      {
        path: '/signin',
        component: signIn
      },
      {
        path: '/signup',
        component: signUp
      }
    ]
  })